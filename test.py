from main import check
import pytest


@pytest.mark.parametrize("n, div, expected", [(1, 0, 1),
                                              (5, 4, 1),
                                              (16, 15, 0),
                                              (57, 56, 0),
                                              (574, 573, 0)])
def test_task_on_correct_result(n, div, expected):
    assert check(n, div) == expected
