def input_num():
    n = int(input("Введите натуральное число: "))
    if n <= 1:
        print("Было получено некорректное значение.")
        input_num()
    else:
        div = n - 1
        check(n, div)

def check(n, div, i=0):
    while div >= 2:
        if n % div == 0:
            print("Число не является простым")
            return i
        else:
            div -= 1
            return check(n, div)
    else:
        print("Число является простым")
        i += 1
        return i

if __name__ == '__main__':
    input_num()